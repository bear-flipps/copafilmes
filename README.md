# Copa do Mundo de Filmes
Para rodar este projeto é necessário apenas que o [Docker](https://www.docker.com/get-started) esteja instalado na sua máquina.


## Tecnologias utilizadas:


    ASP.NET Core 2.2.300
    Nodejs v10.15.3
    Reactjs ^16.8.6
## Para inicializar o projeto:


    $ docker-compose build


## Para rodar o projeto:


    $ docker-compose up

Então acesse no seu browser a url:


    frontend: http://localhost:3002/
    backend: http://localhost:3001/

