using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmesApi.Controllers
{
    public class FilmeModel
    {
        public FilmeModel(string id, string titulo, int ano, double nota)
        {
            Id = id;
            Titulo = titulo;
            Ano = ano;
            Nota = nota;
        }

        public static List<FilmeModel> SelecionaVencedores(List<FilmeModel> filmes) 
        {

            if(filmes.Count <= 2) {
            return filmes
                .OrderByDescending(filme => filme.Nota)
                .ThenBy(filme => filme.Titulo)
                .ToList();
            }

            var listaCount = filmes.Count / 2;

            var primeiraChave = filmes.Take(listaCount).ToList();
            var segundaChave = filmes.Skip(listaCount).Reverse().ToList();

            var vencedores = new List<FilmeModel>();

            for(var i = 0; i < listaCount; i++) {
                var novaLista = new [] { primeiraChave[i], segundaChave[i] };

                var vencedor = novaLista
                    .OrderByDescending(filme => filme.Nota)
                    .ThenBy(filme => filme.Titulo)
                    .First();

                vencedores.Add(vencedor);
            }

            return SelecionaVencedores(vencedores);
        }
        
        public string Id { get; set; }
        public string Titulo { get; set; }
        public int Ano { get; set; }
        public double Nota { get; set; }

    }
}
