﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Linq;

namespace FilmesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilmesController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public FilmesController (IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        // public List<FilmeModel> SelecionaVencedores(List<FilmeModel> filmes) 
        // {

        //     if(filmes.Count <= 2) {
        //     return filmes
        //         .OrderByDescending(filme => filme.Nota)
        //         .ThenBy(filme => filme.Titulo)
        //         .ToList();
        //     }

        //     var listaCount = filmes.Count / 2;

        //     var primeiraChave = filmes.Take(listaCount).ToList();
        //     var segundaChave = filmes.Skip(listaCount).Reverse().ToList();

        //     var vencedores = new List<FilmeModel>();

        //     for(var i = 0; i < listaCount; i++) {
        //         var novaLista = new [] { primeiraChave[i], segundaChave[i] };

        //         var vencedor = novaLista
        //             .OrderByDescending(filme => filme.Nota)
        //             .ThenBy(filme => filme.Titulo)
        //             .First();

        //         vencedores.Add(vencedor);
        //     }

        //     return SelecionaVencedores(vencedores);
        // }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var client = _httpClientFactory.CreateClient();
            var response = await client.GetAsync("https://copadosfilmes.azurewebsites.net/api/filmes");

            string conteudo = await response.Content.ReadAsStringAsync();
            dynamic resultado = JsonConvert.DeserializeObject(conteudo);

            return Ok(resultado);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]List<FilmeModel> model)
        {
            if (model == null || model.Count > 8 || model.Count < 8)
                return BadRequest("É obrigatório o envio de 8 filmes.");

            return Ok(FilmeModel.SelecionaVencedores(model));
        }
    }
}
