using System;
using Xunit;
using FluentAssertions;
using FakeItEasy;
using System.Collections.Generic;
using FilmesApi.Controllers;

namespace CopaFilmes.Testes
{

    public class SelecionaVencedoresTestes
    {
        [Fact]
        public void Deve_Retornar_Uma_Lista_Com_Dois_Vencedores()
        {
            var lista = new List<FilmeModel>();

            lista.Add(new FilmeModel ( "tt3606756", "Os Incríveis 2", 2018, 8.5 ));
            lista.Add(new FilmeModel ( "tt4881806", "Jurassic World: Reino Ameaçado", 2018,  6.7 ));
            lista.Add(new FilmeModel ( "tt5164214", "Oito Mulheres e um Segredo", 2018,  6.3 ));
            lista.Add(new FilmeModel ( "tt7784604", "Hereditário", 2018,  7.8 ));
            lista.Add(new FilmeModel ( "tt4154756", "Vingadores: Guerra Infinita", 2018,  8.8 ));
            lista.Add(new FilmeModel ( "tt5463162", "Deadpool 2", 2018,  8.1 ));
            lista.Add(new FilmeModel ( "tt3778644", "Han Solo: Uma História Star Wars", 2018, 7.2 ));
            lista.Add(new FilmeModel ( "tt3501632", "Thor: Ragnarok",  2017,  7.9 ));

            var resultadoCampeonato = FilmeModel.SelecionaVencedores(lista);

            resultadoCampeonato.Should().NotBeEmpty();
            resultadoCampeonato[0].Id.Should().Be("tt4154756");
            resultadoCampeonato[1].Id.Should().Be("tt5463162");

        }
    }
}

