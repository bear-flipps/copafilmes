import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import axios from 'axios'
import { act } from 'react-dom/test-utils';

jest.mock('axios')

it('renders without crashing', () => {
  axios.get.mockImplementation(() => Promise.resolve({ data: {}}));
  const div = document.createElement('div');
  act(() => {
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});
