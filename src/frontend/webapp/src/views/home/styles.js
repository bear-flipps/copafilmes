import styled from 'styled-components';
import { COLORS } from '../../config/colors';

export const Container = styled.section`
    width: 100%;
    height: 100%;
    max-width: 1920px;
    margin: 0 auto;
    padding: 30px;
`;

export const Aside = styled.div`
    background-color: ${ COLORS.accent };
    padding: 30px;
    width: 100%;
    max-width: 1920px;
    margin: 0 auto 30px;
    text-align: center;
`;

export const LogoType = styled.h2`
    font-size: 2.25rem;
    color: ${ COLORS.purple };
    text-transform: uppercase;
    line-height: 2.25rem;
`;

export const TextSeparator = styled.div`
    position: relative;
    background-color: ${ COLORS.purple };
    height: 5px;
    width: 35%;
    max-width: 470px;
    margin: 0 auto;
`;

export const TextContent = styled.p`
    margin: 4.5rem 0;
    font-size: 1.3rem;
    color: ${ COLORS.purple }
`;

