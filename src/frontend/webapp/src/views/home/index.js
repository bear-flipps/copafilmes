import React, { useState, useEffect } from 'react'
import logo from '../../logo.png';
import Cards from '../../components/cards/';
import Winner from '../../components/winner/';

import { 
    Container, 
    Aside, 
    LogoType,
    TextSeparator,
    TextContent,
} from './styles';

function HomeView() {
    const [content, setContent] = useState('Selecione 8 filmes que você deseja que entrem na competição e depois pressione o botão GERAR MEU CAMPEONATO para prosseguir.');
    const [winner, setWinner] = useState([]);

    function getWinner(winners) {
        setWinner(winners);
    }

    useEffect(() => {
        if(winner.length > 0) {
            setContent('Veja o resultado final do Campeonato de Filmes de forma simples e rápida');
        }
    }, [winner]);

    return (
        <Container>
            <Aside>
                <img src={ logo } alt=""/>
                <LogoType>campeonato de filmes</LogoType>
                <TextSeparator />
                <TextContent>
                    { content }
                </TextContent>
            </Aside>
            { winner.length > 0 ? <Winner winnersList={ winner }/> : <Cards cardsCallback={ getWinner }/> }
        </Container>
    )
}

export default HomeView;