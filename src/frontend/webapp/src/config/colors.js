export const COLORS = {
    accent: '#e8e8fb',
    purple: '#a1a1c4',
    darkPurple: '#4b008a',
    green: '#07be8c',
    darkGreen: '#05805e',
}