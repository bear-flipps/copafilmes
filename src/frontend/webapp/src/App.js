import React from 'react';
import './App.css';
import HomeView from './views/home';

function App() {
  return (
    <div className="App">
      <HomeView />
    </div>
  );
}

export default App;
