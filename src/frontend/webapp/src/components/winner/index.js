import React from 'react';
import WinnersListItem from '../listItem/';
import { WinnersList } from './styles';

export default function Winner({ winnersList }) {
    return(
        <>  
            <WinnersList>
                { winnersList.map((item, i) => (
                    <WinnersListItem key={ item.id } title={ item.titulo } order={ i + 1 }/>
                ))}
            </WinnersList>
        </>
    )
}