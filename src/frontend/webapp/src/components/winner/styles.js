import styled from 'styled-components';

export const Container = styled.div`
    position: relative;
`;

export const WinnersList = styled.ul`
    list-style: none;
    padding: 0;
`;