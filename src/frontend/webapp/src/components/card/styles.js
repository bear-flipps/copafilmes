import styled from 'styled-components';
import { COLORS } from '../../config/colors';

export const Titulo = styled.div`
    font-size: 1.5rem;
    line-height: 1.5rem;
    font-weight: 800;
    color: ${ COLORS.purple };
`;

export const Ano = styled.div`
    font-size: 1.5rem;
    color: ${ COLORS.purple };
`;

export const ItemCard = styled.li`
    background-color: ${ COLORS.accent };
    transition: all 0.2s ease-out;
    flex-basis: 24%;
    max-width: 24%;
    border-radius: 10px;
    padding: 20px 32px;
    box-shadow: 4px 3px 11px -2px rgba(0,0,0,0.4);
    min-height: 150px;
    display: flex;
    flex-flow: column;
    justify-content: space-between;
    margin-bottom: 1%;
    cursor: pointer;

    &:hover {
        background-color: ${ COLORS.darkPurple };
        ${ Titulo } {
            color: white;
        }
        ${ Ano } {
            color: white;
        }
    }

    @media (max-width: 1023px) {
        flex-basis: 100%;
        max-width: 100%;
        margin-bottom: 10%;
    }
`;

export const Nota = styled.div`
    display: none;
`;