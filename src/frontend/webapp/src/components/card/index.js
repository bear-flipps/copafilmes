import React from 'react';
import { 
    ItemCard,
    Titulo,
    Ano,
    Nota,
} from './styles';

export default function Card({ action, id, titulo, ano, nota }) {
    return (
        <ItemCard onClick={ action }>
            <Titulo>{ titulo }</Titulo>
            <Ano>{ ano }</Ano>
            <Nota>{ nota }</Nota>
            <Nota>{ id }</Nota>
        </ItemCard>
    )
}