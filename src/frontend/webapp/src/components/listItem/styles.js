import styled from 'styled-components';
import { COLORS } from '../../config/colors';

export const Winner = styled.li`
    color: ${ COLORS.darkPurple };
    font-size: 1.75rem;
    margin: 10px 0;
    display: flex;

    & > :first-child {
        background-color: ${ COLORS.darkPurple };
        color: ${ COLORS.accent };
        padding: 0px 20px;
    }

    & > :nth-child(2) {
        background-color: ${ COLORS.purple };
        padding: 0px 20px;
        color: white;
    }
`;