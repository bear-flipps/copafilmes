import React from 'react';
import { 
    Winner,
} from './styles';

export default function ListItem({ title, order }) {
    return(
        <Winner>
            <div>{ order }º</div>
            <div>{ title }</div>
        </Winner>
    )
}