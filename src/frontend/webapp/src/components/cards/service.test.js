import fetchData from './service';
import config from '../../config';
import mockAxios from 'axios';

describe('Testa fetchData', () => {
    it('Acessa api e retorna lista de filmes', async () => {
        mockAxios.get.mockImplementationOnce(() => Promise.resolve({
            data: [
                { id: "tt4154756", titulo: "Vingadores: Guerra Infinita", ano: 2018, nota: 8.8  }
            ]
        }));

        const lista = await fetchData(config.getUrl('/api/filmes'));

        expect(lista).toEqual([ { id: "tt4154756", titulo: "Vingadores: Guerra Infinita", ano: 2018, nota: 8.8  } ]);
        expect(mockAxios.get).toHaveBeenCalledTimes(1);
        expect(mockAxios.get).toHaveBeenCalledWith(config.getUrl('/api/filmes'));
    })
})