import axios from 'axios';

async function fetchData(url) {
    const result = await axios.get(url);
    return result.data;
}

export default fetchData;