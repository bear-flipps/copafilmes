import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Card from '../card/';
import Button from '../sendButton/';
import Loading from '../../spinner.svg';
import config from '../../config';
import fetchData from './service';

import { 
    CardsList, 
    Contador, 
    ContadorContainer,
    LoadingImg, 
} from './styles';

export default function Cards({ cardsCallback }) {
    const [data, setData] = useState([]);
    const [count, setCount] = useState(0);
    const [winner, setWinner] = useState([]);

    async function postData(url, data) {
        const result = await axios.post(url, data);
        setWinner(result.data);
    }

    function filterByClassName(item) {
        return item.classList.contains('selected');
    }

    function handleSubmit(e) {
        if(data.length > 0 && count === 8) {
            const optionsList = [...document.querySelectorAll('li')];
            const filteredList = optionsList.filter(filterByClassName);
            e.currentTarget.setAttribute('disabled', true);

            const data = filteredList.map(item => {
                return { 
                    titulo: item.children[0].innerHTML,
                    ano: item.children[1].innerHTML,
                    nota: Number(item.children[2].innerHTML),
                    id: item.children[3].innerHTML,
                }
            });

            postData(config.getUrl('/api/filmes'), data);

        }
    }

    function selectMovie(target) {

        if (count <= 7 && !target.classList.contains('selected')) {
            setCount(count + 1);
            target.classList.add('selected');
        } else if(target.classList.contains('selected')) {
            setCount(count - 1);
            target.classList.remove('selected');
        }

        return;
    }

    function handleClick(e) {
        e.preventDefault();
        selectMovie(e.currentTarget);
    }

    useEffect(() => {
        fetchData(config.getUrl('/api/filmes'))
            .then(response => {
                setData(response);
            })
    }, []);

    useEffect(() => {
        cardsCallback(winner);
    }, [winner, cardsCallback]);

    return(
        <>  
            <ContadorContainer>
                <Contador>Selecionados <span>{ count }</span> filmes de 8</Contador>
                <Button action={ handleSubmit }/>
            </ContadorContainer>
            <CardsList>
                {data.length > 0 ? data.map(item => (
                    <Card
                        key={ item.id }
                        id={ item.id }
                        titulo={ item.titulo }
                        ano={ item.ano }
                        action={ handleClick }
                        nota={ item.nota }
                    />
                )) : <LoadingImg src={ Loading } alt="loading..." />}
            </CardsList>
        </>
    )
}