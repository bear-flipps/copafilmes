import styled from 'styled-components';
import { COLORS } from '../../config/colors';

export const ContadorContainer = styled.div`
    justify-content: space-between;
    position: relative;
    margin: 20px 0;
    display: flex;

    @media (max-width: 1024px) {
        flex-flow: column;
    }
`;

export const CardsList = styled.ul`
    justify-content: space-between;
    align-items: flex-start;
    flex-flow: wrap;
    list-style: none;
    display: flex;
    padding: 30px;
    margin: 0;

    @media (max-width: 1024px) {
        padding: 0;
    }
`;

export const Contador = styled.div`
    color: ${ COLORS.purple };
    font-size: 1.5rem;
`;

export const LoadingImg = styled.img`
    position: absolute;
    margin: 0 auto;
    right: 0;
    left: 0;
`;