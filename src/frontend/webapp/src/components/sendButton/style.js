import styled from 'styled-components';
import { COLORS } from '../../config/colors';

export const Button = styled.button`
    background-color: ${ COLORS.green };
    transition: all 0.2s;
    border-radius: 10px;
    padding: 1rem 2rem;
    font-size: 1.5rem;
    border: none;
    color: white;
    position: relative;
    cursor: pointer;

    &:hover {
        background-color: ${ COLORS.darkGreen };
    }
`;