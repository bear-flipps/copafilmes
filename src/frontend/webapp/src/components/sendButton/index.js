import React from 'react';
import { Button } from './style';

export default function SendButton({ action }) {
    return(
        <Button onClick={ action }>GERAR MEU CAMPEONATO</Button>
    );
}